from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse, Http404
from django.shortcuts import redirect, get_object_or_404, reverse, HttpResponseRedirect
from django.template import loader
from django.views import generic
from django.utils import timezone
from datetime import datetime
from MyApp.models import Person, Question, Choice


def hello(request):
    today = datetime.now().date()
    t = 1
    template = loader.get_template('template/SyrianCinematicClub.html')
    return HttpResponse(t, template.render(request))


def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'app_1/detail.html', {'question' : question})


def result(request, question_id):
    response = "You're Looking for Question %s"
    return HttpResponse(response % question_id)


def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'app_1/results.html', {'question': question})


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'app_1/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice. "
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
    return HttpResponseRedirect(reverse('app_1:results', args=(question.id, )))


def test(request, number):
    text = "Displaying Number %s " %number
    return HttpResponse(text)


def index(request):
    question_latest_list = Question.objects.order_by('-pub_date')[:5]
    template = loader.get_template("app_1/index.html")
    context = {
        'question_latest_list': question_latest_list,
    }
    return HttpResponse(template.render(context, request))


class IndexView(generic.ListView):
    template_name = 'app_1/index.html'
    context_object_name = 'question_latest_list'

    def get_queryset(self):
        return Question.objects.filter(
            pub_date=timezone.now()
        ).order_by('-pub_date')[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = 'app_1/detail.html'

    def get_queryset(self):

        return Question.objects.filter(pub_date__lte=timezone.now())


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'app_1/results.html'


def crudops(request):
    p = Person(name="Fred Flintstone", shirt_size="L")
    p.save()
    print(Person.name)


def redirect_url(request):
    return redirect("http://www.djangoproject.com")


def redirect_article(request, year, day):
    text = "Trying to redirect fto another page with parameters: %s/%s" %(year, day)
    return HttpResponse(text)


def article_id(request, article_ID):
    text = "another way to redirect with a parameter %s" %article_ID
    return redirect('/redirect_article/2/5/')
