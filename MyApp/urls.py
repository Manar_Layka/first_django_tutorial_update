from django.urls import path, include
from . import views

app_name = 'app_1'
urlpatterns = [
    path('', views.IndexView.as_view(), name="index"),
    path('hello/', views.hello),
    path('<int:pk>/', views.DetailView.as_view(), name="detail"),
    path('result/<int:question_id>/', views.result, name="result"),
    path('<int:question_id>/vote/', views.vote, name='vote'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name="results"),
    path('test/<int:number>/', views.test),
    path('index/', views.index),
    path('crudops/', views.crudops),
    path('redirect_url/', views.redirect_url),
    path('redirect_article/<int:year>/<int:day>/', views.redirect_article),
    path('article_id/<int:article_ID>/', views.article_id),
    #path('<int:question_id>/', views.detail, name='detail'),
]