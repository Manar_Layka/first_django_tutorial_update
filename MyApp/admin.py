from django.contrib import admin

# Register your models here.

from django.contrib import admin

from .models import Question, Choice, Article

#admin.site.register(models.Article)
#admin.site.register(models.Question)


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                {'fields' : ['question_text']}),
        ('Date Information',  {'fields' : ['pub_date']})
    ]
    inlines = [ChoiceInline]
    list_display = ('question_text', 'pub_date', 'was_published_recently')


class ArticleAdmin(admin.ModelAdmin):
    fields = ['Article Text']


admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice)
admin.site.register(Article)